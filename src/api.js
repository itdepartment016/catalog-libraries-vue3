const API_URL = "/data-53-structure-3.json";

const getResource = async () => {
  const res = await fetch(API_URL);
  const body = await res.json();
  return body;
};

export const getAllLibraries = (cb) => {
  getResource().then((libraries) => {
    cb(libraries);
  });
};
export const getLibrary = (cb, id) => {
  getResource().then((libraries) => {
    const library = libraries.filter((item) => item._id.includes(id));
    cb(library[0]);
  });
};
