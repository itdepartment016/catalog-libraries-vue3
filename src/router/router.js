import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import SearchLibraries from "@/views/SearchLibraries.vue";
import LibFull from "@/views/LibFull.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/libraries",
    name: "Libraries",
    component: SearchLibraries,
  },
  {
    path: "/libraries/:id",
    name: "LibFull",
    component: LibFull,
  },

];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
